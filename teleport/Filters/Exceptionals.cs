﻿using System.Text.RegularExpressions;

namespace Teleport.Filters
{
    public class Exceptionals
    {
        /// <summary>
        /// For some reason, Python creates 10s of registry entries for its idiotic self.
        /// </summary>
        /// <param name="pyEntry">Name of the installed python component</param>
        /// <returns>Cleaned name</returns>
        public static string PurgePython(string pyEntry)
        {
            if (pyEntry.Contains("Python"))
            {
                string pattern = @"Python \d+\.\d+";
                Match match = Regex.Match(pyEntry, pattern);
                if (match.Success)
                {
                    return match.Value;
                }
            }
            return pyEntry.Trim();
        }

        /// <summary>
        /// Remove uninstallers from cleaned output.
        /// </summary>
        /// <param name="entry">Uninstaller name</param>
        /// <returns>Cleaned name</returns>
        public static string PurgeUninstaller(string entry)
        {
            if (entry.Contains("uninstall", StringComparison.CurrentCultureIgnoreCase))
            {
                string toIgnore = "Uninstall";
                if (entry.Contains("uninstaller", StringComparison.CurrentCultureIgnoreCase))
                {
                    toIgnore += "er";
                }
                return entry.Replace(toIgnore, "");
            }
            return entry;
        }

        /// <summary>
        /// Remove the version number from a "Qt" string.
        /// </summary>
        /// <param name="qtEntry">Name of the Qt component</param>
        /// <returns>Cleaned name without the version number</returns>
        public static string PurgeQtVersion(string qtEntry)
        {
            if (qtEntry.StartsWith("Qt "))
            {
                string pattern = @"^Qt \d+(\.\d+)*";
                Match match = Regex.Match(qtEntry, pattern);
                if (match.Success)
                {
                    return "Qt";
                }
            }
            return qtEntry.Trim();
        }
    }
}
