﻿using System.Text;
using System.Text.RegularExpressions;

namespace Teleport.Filters
{
    public static partial class Texts
    {
        private static readonly char[] separators = [' ', '\t', '\n', '\r'];

        /// <summary>
        /// Remove non-ascii characters from string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveNonAscii(string input)
        {
            StringBuilder output = new();
            foreach (char c in input)
            {
                if (c < 128)
                    output.Append(c);
            }
            return output.ToString();
        }

        /// <summary>
        /// Remove brackets and their content
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveBrackets(string input)
        {
            int startIndex = input.IndexOf('(');
            if (startIndex != -1)
            {
                int endIndex = input.IndexOf(')', startIndex);
                if (endIndex != -1)
                {
                    return input.Remove(startIndex, endIndex - startIndex + 1).Trim();
                }
            }
            return input;
        }

        /// <summary>
        /// Removes entries containing any of the specified substrings from the list.
        /// </summary>
        /// <param name="entries">The list of entries.</param>
        /// <param name="substrings">The list of substrings to exclude.</param>
        /// <returns>The filtered list.</returns>
        public static List<string> RemoveEntriesContainingSubstrings(List<string> entries, List<string> substrings)
        {
            return entries.Where(entry => !substrings.Any(substring => entry.Contains(substring))).ToList();
        }

        /// <summary>
        /// Remove first word from string
        /// </summary>
        /// <param name="name">The string</param>
        /// <returns>Null if nothing, first word if something</returns>
        public static string? RemoveFirstWord(string name)
        {
            return name.Split(separators, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
        }

        /// <summary>
        /// Remove everything but the first word.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>If empty, an empty string, otherwise the truncation</returns>
        public static string? RemoveAllButFirstWord(string name)
        {
            string res = name.Split(
                separators, StringSplitOptions.RemoveEmptyEntries
            ).FirstOrDefault() ?? string.Empty;
            if (!string.IsNullOrEmpty(res))
            {
                return res.Trim();
            }
            return string.Empty;
        }

        /// <summary>
        /// Remove version number from string (typically a program name).
        /// </summary>
        /// <param name="name">Name of program with version</param>
        /// <returns>Name of program without version</returns>
        public static string RemoveVersionNumber(string name) => VersionRemovalRegex().Replace(name, "");

        [GeneratedRegex(@"\s+\d+(\.\d+)*$")]
        private static partial Regex VersionRemovalRegex();
    }
}
