﻿using Teleport.Exceptions;

namespace Teleport.Web
{
    public class DWebClient
    {
        private readonly HttpClient client;
        private readonly Random random;

        private readonly string[] UserAgents = [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
        ];

        public DWebClient()
        {
            random = new Random();
            client = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(4)
            };
        }

        public async Task<string> Get(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("User-Agent", GetUserAgent());
            try
            {
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                ErrorService.Log("Request problem", e, url);
                return string.Empty;
            }
        }

        public async Task DownloadFile(string url, string filePath)
        {
            var uri = new Uri(url);
            var result = await client.GetAsync(uri);
            using var fs = new FileStream(filePath, FileMode.CreateNew);
            await result.Content.CopyToAsync(fs);
        }

        private string GetUserAgent()
        {
            int index = random.Next(UserAgents.Length);
            return UserAgents[index];
        }
    }
}