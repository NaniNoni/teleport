﻿using AngleSharp.Html.Parser;

namespace Teleport.Web
{
    public class SearchResult
    {
        public List<PageResult> Pages { get; set; }
        public List<RelatedSearch> RelatedSearches { get; set; }
        public List<AlsoTry> AlsoTry { get; set; }
        public Dictionary<string, object> Card { get; set; }
    }

    public class PageResult
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Text { get; set; }
    }

    public class RelatedSearch
    {
        public string Text { get; set; }
        public string Link { get; set; }
    }

    public class AlsoTry
    {
        public string Text { get; set; }
        public string Link { get; set; }
    }

    public static class Searcher
    {
        private static readonly string YahooSearchUrl = "https://sg.search.yahoo.com/search?q=";

        public static async Task<SearchResult> SearchAsync(string query)
        {
            using HttpClient client = new HttpClient();
            string searchUrl = YahooSearchUrl + Uri.EscapeUriString(query);
            string html = await client.GetStringAsync(searchUrl);
            return ParseSearchResult(html);
        }

        private static SearchResult ParseSearchResult(string html)
        {
            var parser = new HtmlParser();
            var document = parser.ParseDocument(html);
            var contents = new SearchResult
            {
                Pages = [],
                RelatedSearches = [],
                AlsoTry = [],
                Card = []
            };

            var search = document.QuerySelector(".reg.searchCenterMiddle");
            if (search == null)
                throw new Exception("Could not find '.reg.searchCenterMiddle' (search results)");

            foreach (var webpage in search.QuerySelectorAll(".dd.algo.algo-sr"))
            {
                var pageResult = new PageResult();

                var title = webpage.QuerySelector("div.compTitle h3 a");
                if (title != null)
                {
                    pageResult.Title = title.TextContent.Trim();
                    pageResult.Link = title.GetAttribute("href");
                }

                var texts = webpage.QuerySelector(".compText.aAbs p");
                if (texts != null)
                    pageResult.Text = texts.TextContent.Trim();

                contents.Pages.Add(pageResult);
            }

            var card = document.QuerySelector(".cardReg.searchRightTop");
            if (card != null)
            {
                var image = card.QuerySelector("img");
                if (image != null)
                    contents.Card["image"] = image.GetAttribute("src");

                var heading2 = card.QuerySelector("p.pl-15.pr-10 span");
                if (heading2 != null)
                    contents.Card["heading"] = heading2.TextContent.Trim();

                var innerContent = card.QuerySelector("div.compText p");
                if (innerContent != null)
                {
                    contents.Card["text"] = innerContent.TextContent.Trim();
                    var source = innerContent.QuerySelector("a");
                    if (source != null)
                    {
                        contents.Card["source"] = new Dictionary<string, string>
                    {
                        { "link", source.GetAttribute("href") },
                        { "text", source.TextContent.Trim() }
                    };
                    }
                }
            }

            foreach (var item in document.QuerySelectorAll("ol.cardReg.searchTop .compDlink li span a"))
            {
                contents.AlsoTry.Add(new AlsoTry
                {
                    Link = item.GetAttribute("href"),
                    Text = item.TextContent.Trim()
                });
            }

            foreach (var item in document.QuerySelectorAll("ol.scf.reg.searchCenterFooter tbody tr td a"))
            {
                contents.RelatedSearches.Add(new RelatedSearch
                {
                    Link = item.GetAttribute("href"),
                    Text = item.TextContent.Trim()
                });
            }
            return contents;
        }
    }
}
