﻿namespace Teleport.Models
{
    [Serializable]
    public enum StatusType
    {
        NONE,
        WINE,
        NATIVE,
        FLATPAK,
        PROTON,
        YABRIDGE
    }
}
