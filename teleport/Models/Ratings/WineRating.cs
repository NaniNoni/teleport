﻿namespace Teleport.Models.Ratings
{
    [Serializable]
    public enum WineRating
    {
        Platinum,
        Gold,
        Silver,
        Bronze,
        Garbage,
        None
    }
}
