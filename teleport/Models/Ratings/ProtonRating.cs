﻿namespace Teleport.Models.Ratings
{
    [Serializable]
    public enum ProtonRating
    {
        Platinum,
        Gold,
        Silver,
        Bronze,
        Borked
    }
}
