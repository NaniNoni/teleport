﻿using Teleport.Models.Ratings;

namespace Teleport.Models
{
    [Serializable]
    public class Status(StatusType statusType)
    {
        public StatusType Type { get; set; } = statusType;
        public ProtonRating? ProtonRating { get; set; }
        public WineRating? WineRating { get; set; }

        public override string ToString()
        {
            switch (Type)
            {
                case StatusType.NATIVE:
                case StatusType.FLATPAK:
                    return "[green]Native[/]";
                case StatusType.WINE:
                    string wm = "Wine";
                    if (WineRating != null)
                    {
                        if (WineRating == Ratings.WineRating.Platinum)
                        {
                            wm = "[white]" + wm + " - Platinum";
                        }
                        else if (WineRating == Ratings.WineRating.Gold)
                        {
                            wm = "[darkgoldenrod]" + wm + " - Gold";
                        }
                        else if (WineRating == Ratings.WineRating.Silver)
                        {
                            wm = "[silver]" + wm + " - Silver";
                        }
                        else if (WineRating == Ratings.WineRating.Bronze)
                        {
                            wm = "[orange4_1]" + wm + " - Bronze";
                        }
                        else if (WineRating == Ratings.WineRating.Garbage)
                        {
                            wm = "[red]" + wm + " - Garbage";
                        }
                        else
                        {
                            wm = "[red]No WineHQ entry found";
                        }
                        wm += "[/]";
                    }
                    return wm;
                case StatusType.PROTON:
                    string pm = "Proton";
                    if (ProtonRating != null)
                    {
                        if (ProtonRating == Ratings.ProtonRating.Platinum)
                        {
                            pm = "[white]" + pm + " - Platinum";
                        }
                        else if (ProtonRating == Ratings.ProtonRating.Gold)
                        {
                            pm = "[darkgoldenrod]" + pm + " - Gold";
                        }
                        else if (ProtonRating == Ratings.ProtonRating.Silver)
                        {
                            pm = "[silver]" + pm + " - Silver";
                        }
                        else if (ProtonRating == Ratings.ProtonRating.Bronze)
                        {
                            pm = "[orange4_1]" + pm + " - Bronze";
                        }
                        else if (ProtonRating == Ratings.ProtonRating.Borked)
                        {
                            pm = "[red]" + pm + " - Borked";
                        }
                        pm += "[/]";
                    }
                    return pm;
                case StatusType.YABRIDGE:
                    return "[darkgoldenrod](May work via yabridge)[/]";
            }
            return "[red]No WineHQ entry found[/]";
        }
    }
}
