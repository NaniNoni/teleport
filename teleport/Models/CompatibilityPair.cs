﻿namespace Teleport.Models
{
    [Serializable]
    public class CompatibilityPair
    {
        public required string Name { get; set; }
        public required Status State { get; set; }
    }
}
