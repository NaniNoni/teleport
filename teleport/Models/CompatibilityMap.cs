﻿using Spectre.Console;
using Teleport.Models.Ratings;

namespace Teleport.Models
{
    [Serializable]
    public class CompatibilityMap
    {
        private readonly List<CompatibilityPair> compatibilityPairs;

        public CompatibilityMap()
        {
            compatibilityPairs = [];
        }

        public void Add(CompatibilityPair pair)
        {
            compatibilityPairs.Add(pair);
        }

        public void Sort()
        {
            compatibilityPairs.Sort((firstPair, secondPair) =>
            {
                // First, compare by ProtonStatus if both are PROTON
                if (firstPair.State.Type == StatusType.PROTON && secondPair.State.Type == StatusType.PROTON)
                {
                    var firstProtonRating = firstPair.State.ProtonRating ?? ProtonRating.Borked;
                    var secondProtonRating = secondPair.State.ProtonRating ?? ProtonRating.Borked;
                    return firstProtonRating.CompareTo(secondProtonRating);
                }

                // Check wine status
                if (firstPair.State.Type == StatusType.WINE && secondPair.State.Type == StatusType.WINE)
                {
                    var fw = firstPair.State.WineRating ?? WineRating.Garbage;
                    var sw = secondPair.State.WineRating ?? WineRating.Garbage;
                    return fw.CompareTo(sw);
                }

                // If not both PROTON, compare by status type
                var order = GetOrder(firstPair.State.Type) - GetOrder(secondPair.State.Type);
                return order != 0 ? order : string.Compare(firstPair.Name, secondPair.Name, StringComparison.Ordinal);
            });
        }

        private static int GetOrder(StatusType type)
        {
            switch (type)
            {
                case StatusType.NATIVE:
                    return 0;
                case StatusType.FLATPAK:
                    return 1;
                case StatusType.PROTON:
                    return 2;
                case StatusType.YABRIDGE:
                    return 3;
                case StatusType.WINE:
                    return 4;
                default:
                    return 5;
            }
        }

        public Table ToTable()
        {
            Sort();
            var table = new Table
            {
                ShowRowSeparators = true
            };
            table.AddColumn("App");
            table.AddColumn("Compatibility");
            foreach (CompatibilityPair pair in compatibilityPairs)
            {
                table.AddRow(pair.Name, pair.State.ToString());
            }
            return table;
        }

        public List<CompatibilityPair> GetCompatibilityPairs()
        {
            return compatibilityPairs;
        }

        public int Count()
        {
            return compatibilityPairs.Count;
        }

    }
}

