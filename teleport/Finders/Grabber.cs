﻿using Microsoft.Win32;
using Teleport.Classification;
using Teleport.Filters;
using Teleport.Local;

namespace Teleport.Finders
{
    public class Grabber
    {
        /// <summary>
        /// Get all installed software on host machine
        /// </summary>
        /// <returns>List of installed software names</returns>
        public static List<string> GetInstalledSoftware()
        {
            string[] keys =
            [
                @"Software\Microsoft\Windows\CurrentVersion\Uninstall",
                @"Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall",
                @"Software\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-18\Products",
                @"Software\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-21-*\Products"
            ];
            List<string> installedSoftware = [];
            HashSet<string> uniqueSoftware = [];

            installedSoftware.AddRange(StartRetriever.Traverse());

            foreach (string keyPath in keys)
            {
                using RegistryKey? key = Registry.LocalMachine.OpenSubKey(keyPath);
                if (key == null)
                    continue;

                foreach (string subkeyName in key.GetSubKeyNames())
                {
                    if (subkeyName == null)
                        continue;

                    using RegistryKey? subkey = key.OpenSubKey(subkeyName);
                    if (subkey == null)
                        continue;

                    if (subkey.GetValue("DisplayName") is not string displayName)
                        continue;

                    displayName = Texts.RemoveBrackets(Texts.RemoveNonAscii(displayName));
                    if (!Constants.ExcludedFromInitialSearch.Any(
                        keyword => displayName.Contains(keyword, StringComparison.CurrentCultureIgnoreCase) ||

                        Constants.Exceptions.Any(
                            exception => displayName.Contains(exception, StringComparison.CurrentCultureIgnoreCase))
                        )
                    )
                    {
                        if (uniqueSoftware.Add(displayName))
                        {
                            installedSoftware.Add(displayName);
                        }
                    }
                }
            }

            List<string> fin = [];

            foreach (string inst in installedSoftware)
            {
                var toAdd = Exceptionals.PurgeQtVersion(Exceptionals.PurgeUninstaller(Exceptionals.PurgePython(inst)));
                string progNameToAdd = Texts.RemoveBrackets(Texts.RemoveNonAscii(toAdd)).Trim();
                if (!Constants.Exceptions.Contains(progNameToAdd.ToLower()))
                {
                    fin.Add(progNameToAdd);
                }
            }

            fin = Texts.RemoveEntriesContainingSubstrings(fin, [.. Constants.ExcludedFromInitialSearch]).Distinct().ToList();

            if (CacheService.VerifyInstalledSoftwareHash(fin))
            {
                Constants.UseCached = true;
            }
            else
            {
                CacheService.StoreHash(fin);
            }

            return fin;
        }
    }
}