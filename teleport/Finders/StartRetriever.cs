﻿using Lnk;
using Teleport.Exceptions;

namespace Teleport.Finders
{
    /// <summary>
    /// Traverses start directory in search of installed programs.
    /// This is to cover unconventional install locations.
    /// </summary>
    public static class StartRetriever
    {
        public static List<string> Traverse()
        {
            List<string> installedSoftware = [];
            HashSet<string> uniqueSoftware = [];
            string startMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
            foreach (string shortcutFile in Directory.GetFiles(startMenuPath, "*.lnk", SearchOption.AllDirectories))
            {
                string programName = Path.GetFileNameWithoutExtension(shortcutFile);
                if (programName != null && !uniqueSoftware.Contains(programName))
                {
                    string targetPath = ResolveShortcut(shortcutFile);
                    if (targetPath != null)
                    {
                        if (!targetPath.Contains("exe", StringComparison.CurrentCultureIgnoreCase) || 
                            targetPath.Contains("uninstall", StringComparison.CurrentCultureIgnoreCase))
                        {
                            continue;
                        }
                        if (!string.IsNullOrEmpty(targetPath) && File.Exists(targetPath))
                        {
                            uniqueSoftware.Add(programName);
                            installedSoftware.Add(programName);
                        }
                    }
                }
            }
            return installedSoftware;
        }

        public static string ResolveShortcut(string shortcutPath)
        {
            var targetPath = string.Empty;
            try
            {
                byte[] shortcutBytes = File.ReadAllBytes(shortcutPath);
                LnkFile lnkFile = new(shortcutBytes, shortcutPath);
                targetPath = lnkFile.LocalPath;
            }
            catch (Exception ex)
            {
                ErrorService.Log("Error resolving shortcut", ex);
            }
            return targetPath;
        }
    }
}
