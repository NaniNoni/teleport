﻿using System.Text.Json;
using Teleport.Classification.Groups;
using Teleport.Exceptions;
using Teleport.Local;
using Teleport.Models;
using Teleport.Models.Ratings;

namespace Teleport.Classification
{
    public class Classifier
    {
        private static readonly Natives nativeChecker = new();
        private ProtonIdentificationService? protonChecker;
        private static JsonDocument? flatpaks;

        public Classifier()
        {
            protonChecker = new ProtonIdentificationService();
        }

        public async Task<CompatibilityPair> ClassifyAsync(string progName)
        {
            ProtonRating? protonRating = null;
            WineRating? wineRating = null;
            flatpaks = await FPHandler.GetFlatpaks();
            StatusType result;

            if (await Natives.IsFlatpakAvailable(progName, flatpaks))
            {
                result = StatusType.FLATPAK;
            }
            else if (await nativeChecker.IsExhaustivelyNativeChecked(progName))
            {
                result = StatusType.NATIVE;
            }
            else if (protonChecker!.IsSteamGame(progName).Result)
            {
                result = StatusType.PROTON;
                ProtonRating? directRating = await protonChecker.GetDirect(progName);
                if (directRating != null)
                {
                    protonRating = directRating;
                }
                else
                {
                    protonRating = protonChecker.GetProtonStatus(progName);
                }
            }
            else if (await nativeChecker.IsPackageAvailable(progName))
            {
                result = StatusType.NATIVE;
            }
            else if (VSTs.IsVstInstalled(progName))
            {
                result = StatusType.YABRIDGE;
            }
            else
            {
                string compatLevel = await WineHandler.GetWineCompatibility(progName);
                if (compatLevel == "vst")
                {
                    result = StatusType.YABRIDGE;
                }
                else
                {
                    result = StatusType.WINE;
                    wineRating = await WineHandler.BindCompatToRating(compatLevel);
                }
            }
            var compatPair = new CompatibilityPair
            {
                Name = progName,
                State = new Status(result)
            };
            if (protonRating != null)
            {
                compatPair.State.ProtonRating = protonRating;
            }
            if (wineRating != null)
            {
                compatPair.State.WineRating = wineRating;
            }
            return compatPair;
        }

        public async Task<CompatibilityMap> ClassifyAllProgramsAsync(List<string> progNames)
        {
            if (Constants.UseCached)
            {
                return CacheService.ReadSavedMap();
            }

            protonChecker = new ProtonIdentificationService();
            var map = new CompatibilityMap();
            var classifyTasks = new List<Task<CompatibilityPair>>();

            foreach (string progName in progNames)
            {
                try
                {
                    classifyTasks.Add(ClassifyAsync(progName));

                } catch (Exception e)
                {
                    ErrorService.Log("Problem in CAProgsa", e);
                }
            }

            var results = await Task.WhenAll(classifyTasks);
            foreach (var result in results)
            {
                map.Add(result);
            }

            if (!Constants.UseCached)
            {
                CacheService.SaveCompatMap(map);
                Constants.UseCached = true;
            }
            return map;
        }
    }
}
