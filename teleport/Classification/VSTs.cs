﻿namespace Teleport.Classification
{
    public static class VSTs
    {
        public static bool IsVstInstalled(string name)
        {
            string VSTPath = @"C:\Program Files\Common Files\VST\";
            name = name.ToLower();
            foreach (var vendor in Constants.CommonVstVendors)
            {
                if (name.Contains(vendor))
                {
                    return true;
                }
            }
            if (Directory.Exists($"{VSTPath}VST2"))
            {
                foreach (var directory in Directory.GetDirectories($"{VSTPath}VST2"))
                {
                    if (Directory.GetFiles(directory, name + ".dll", SearchOption.AllDirectories).Length > 0)
                    {
                        return true;
                    }
                }
            }
            if (Directory.Exists(@"C:\Program Files\Common Files\VST3"))
            {
                foreach (var directory in Directory.GetDirectories(@"C:\Program Files\Common Files\VST3"))
                {
                    if (Directory.GetFiles(directory, name + ".vst3", SearchOption.AllDirectories).Length > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
