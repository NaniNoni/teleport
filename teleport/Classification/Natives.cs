﻿using AngleSharp.Common;
using System.Text.Json;
using Teleport.Exceptions;
using Teleport.Filters;
using Teleport.Web;

namespace Teleport.Classification
{
    public class Natives
    {
        private readonly DWebClient client = new();

        public async Task<bool> IsPackageAvailable(string packageName)
        {
            packageName = packageName.Trim();
            if (IsCommonPackage(packageName))
            {
                return true;
            }
            packageName = Uri.EscapeDataString(packageName.ToLower().Split(' ')[0]).Replace(" ", "+");
            string urlDeb = $"https://packages.debian.org/search?keywords={packageName}";
            string responseDeb = string.Empty;
            try
            {
                responseDeb = await client.Get(urlDeb);
            }
            catch (Exception e) 
            {
                Console.WriteLine("IsPackageAvailable Issue: " + e.ToString());
            }
            return responseDeb.Contains("Exact hits");
        }

        public static bool IsCommonPackage(string packageName)
        {
            return Constants.Common.Contains(packageName)
                || Constants.Common.Contains(Texts.RemoveFirstWord(packageName));
        }

        public static Task<bool> IsFlatpakAvailable(string progName, JsonDocument flatpaks)
        {
            foreach (var app in flatpaks.RootElement.EnumerateArray())
            {
                string name = app.GetProperty("name").GetString()!;
                var progger = progName.Split()[0].Trim();
                if (name == progger || name == progName)
                {
                    return Task.FromResult(true);
                }
            }
            return Task.FromResult(false);
        }

        private async Task<bool> IsMentionedAtAllForNative(string baseName)
        {
            string response;
            try
            {
                response = await client.Get($"https://{Texts.RemoveAllButFirstWord(baseName)}.com");
            }
            catch (Exception ex)
            {
                ErrorService.Log("Issue in IMATFN", ex);
                return false;
            }
            return response.Contains("linux", StringComparison.CurrentCultureIgnoreCase);
        }

        public async Task<bool> IsExhaustivelyNativeChecked(string progName)
        {
            SearchResult result = await Searcher.SearchAsync($"{progName}");
            try
            {
                PageResult progPage = result.Pages.GetItemByIndex(0);
                if (progPage.Text.Contains("linux", StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return await IsMentionedAtAllForNative(progName);
                }
            }
            catch (Exception ex)
            {
                ErrorService.Log("Problem in native check", ex);
                return false;
            }
        }
    }
}
