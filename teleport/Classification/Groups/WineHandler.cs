﻿using Teleport.Models.Ratings;
using Teleport.Web;

namespace Teleport.Classification.Groups
{
    public static class WineHandler
    {
        private static readonly DWebClient client = new();

        public static async Task<string> GetWineCompatibility(string appName)
        {
            string lAppName = appName.ToLower();
            string failMsg = "WineHQ rating not found";

/*          string[] commonWineMisconceptions = ["misconception1", "misconception2"];
            string[] commonWine = ["app1", "app2"];

            foreach (string wineMisconception in commonWineMisconceptions)
            {
                if (lAppName.Contains(wineMisconception))
                {
                    return failMsg;
                }
            }

            if (commonWine.Contains(appName.Trim().ToLower()))
            {
                return "WineHQ - " + commonWine.First(w => w.Equals(appName.Trim().ToLower(), StringComparison.OrdinalIgnoreCase));
            }
 */
            var result = await Searcher.SearchAsync(appName + " winehq");
            var winehqUrl = result.Pages.FirstOrDefault(page => page.Link.Contains("winehq", StringComparison.CurrentCultureIgnoreCase))?.Link;

            if (!string.IsNullOrEmpty(winehqUrl))
            {
                if (winehqUrl.Contains("objectManager"))
                {
                    string? html = await client.Get(winehqUrl);
                    if (!string.IsNullOrEmpty(html))
                    {
                        html = html.ToLower();
                        string[] ratingClasses = ["platinum", "gold", "silver", "bronze", "garbage"];
                        foreach (var vstSite in Constants.VstSites)
                        {
                            if (html.Contains(vstSite))
                            {
                                return "vst";
                            }
                        }
                        foreach (var ratingClass in ratingClasses)
                        {
                            if (html.Contains(ratingClass))
                            {
                                return ratingClass;
                            }
                        }
                    }
                }
            }
            return failMsg;
        }

        public static Task<WineRating> BindCompatToRating(string compatLevel)
        {
            return Task.FromResult(compatLevel switch
            {
                "platinum" => WineRating.Platinum,
                "gold" => WineRating.Gold,
                "silver" => WineRating.Silver,
                "bronze" => WineRating.Bronze,
                "garbage" => WineRating.Garbage,
                _ => WineRating.None,
            });
        }
    }
}
