﻿using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using System.Text.Json;
using Teleport.Exceptions;
using Teleport.Models.Ratings;
using Teleport.Web;

namespace Teleport.Classification.Groups
{
    public partial class ProtonIdentificationService
    {
        private readonly string protonReportUrl = "https://github.com/bdefore/protondb-data/raw/master/reports/reports_may5_2024.tar.gz";
        private readonly string tempDirectory = Path.Combine(Path.GetTempPath(), "pdb_temp_");
        private string pathOfReportArchive = string.Empty;
        private string pathOfJson = Path.Combine(Path.Combine(Path.GetTempPath(), "pdb_temp_"), "reports_piiremoved.json");
        private JsonDocument? pdb = null;
        private readonly DWebClient client = new();
        private readonly List<string> negativeYes = [
                "performanceFaults",
                "significantBugs",
                "inputFaults",
                "stabilityFaults",
                "windowingFaults",
                "graphicalFaults",
                "isImpactedByAntiCheat",
                "saveGameFaults"
        ];
        private readonly List<string> positiveYes = [
                "installs",
                "batteryPerformance",
                "opens",
                "startsPlay",
                "verdict"
        ];

        public ProtonIdentificationService()
        {
            pathOfReportArchive = Path.Combine(tempDirectory, "reports_may5_2024.tar.gz");
            _ = DownloadProtonReport();
            int attempts = 0;
            while (attempts < 5)
            {
                if (Path.Exists(pathOfReportArchive))
                {
                    ExtractAndSetReport();
                    if (Path.Exists(pathOfJson))
                    {
                        ErrorService.LogInfo("Proton ID Service: JSON exists");
                        break;
                    }
                    attempts++;
                }
            }
        }

        public async Task DownloadProtonReport()
        {
            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);

            if (!File.Exists(pathOfReportArchive))
            {
                await client.DownloadFile(protonReportUrl, pathOfReportArchive);
            }
        }

        private void SetJson()
        {
            string? jsonFile = Directory.GetFiles(tempDirectory, "*.json").FirstOrDefault();
            if (jsonFile != null)
            {
                pathOfJson = jsonFile;
                using var fileStream = File.OpenRead(jsonFile);
                pdb = JsonDocument.Parse(fileStream);
            }
        }

        public async void ExtractAndSetReport()
        {
            if (pathOfJson != string.Empty)
            {
                SetJson();
            }
            if (File.Exists(pathOfReportArchive))
            {
                if (!File.Exists(pathOfJson))
                {
                    using Stream inStream = File.OpenRead(pathOfReportArchive);
                    using Stream gzipStream = new GZipInputStream(inStream);
                    TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
                    tarArchive.ExtractContents(tempDirectory);
                }
                SetJson();
            }
            else
            {
                await DownloadProtonReport();
                ExtractAndSetReport();
            }
        }

        public ProtonRating GetProtonStatus(string gameTitle)
        {
            int score = 0;

            try
            {
                if (pdb == null)
                {
                    SetJson();
                }

                foreach (JsonElement entry in pdb!.RootElement.EnumerateArray())
                {
                    if (entry.TryGetProperty("app", out JsonElement appElement) &&
                            appElement.TryGetProperty("title", out JsonElement titleElement) &&
                            titleElement.GetString() == gameTitle)
                    {
                        if (entry.TryGetProperty("responses", out JsonElement respElement))
                        {
                            //Positive yes
                            respElement.TryGetProperty("installs", out JsonElement installs);
                            respElement.TryGetProperty("opens", out JsonElement opens);
                            respElement.TryGetProperty("startsPlay", out JsonElement startsPlay);
                            respElement.TryGetProperty("verdict", out JsonElement verdict);

                            //Negative yes
                            respElement.TryGetProperty("performanceFaults", out JsonElement performanceFaults);
                            respElement.TryGetProperty("significantBugs", out JsonElement significantBugs);
                            respElement.TryGetProperty("inputFaults", out JsonElement inputFaults);
                            respElement.TryGetProperty("stabilityFaults", out JsonElement stabilityFaults);
                            respElement.TryGetProperty("windowingFaults", out JsonElement windowingFaults);
                            respElement.TryGetProperty("graphicalFaults", out JsonElement graphicalFaults);
                            respElement.TryGetProperty("isImpactedByAntiCheat", out JsonElement isImpactedByAntiCheat);
                            respElement.TryGetProperty("saveGameFaults", out JsonElement saveGameFaults);

                            score = GetScore(new Dictionary<string, JsonElement>
                            {
                                { "installs", installs },
                                { "opens", opens },
                                { "startsPlay", startsPlay },
                                { "verdict", verdict },
                                { "performanceFaults", performanceFaults },
                                { "significantBugs", significantBugs },
                                { "inputFaults", inputFaults },
                                { "stabilityFaults", stabilityFaults },
                                { "windowingFaults", windowingFaults },
                                { "graphicalFaults", graphicalFaults },
                                { "isImpactedByAntiCheat", isImpactedByAntiCheat },
                                { "saveGameFaults", saveGameFaults }
                            });

                            int contentCount = new List<JsonElement>
                            {
                                installs,
                                opens,
                                startsPlay,
                                verdict,
                                performanceFaults,
                                significantBugs,
                                inputFaults,
                                stabilityFaults,
                                windowingFaults,
                                graphicalFaults,
                                isImpactedByAntiCheat,
                                saveGameFaults
                            }.Count(element => element.ValueKind == JsonValueKind.String);

                            if (contentCount < 5)
                            {
                                continue;
                            }
                            else
                            {
                                if (verdict.ValueKind == JsonValueKind.String)
                                {
                                    if (verdict.GetString()!.Trim() == "no")
                                    {
                                        return ProtonRating.Borked;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            } catch(Exception ex)
            {
                ErrorService.Log("Error in GetProtonStatus", ex);
            }
            return RateFromScore(score);
        }

        private static ProtonRating RateFromScore(int score)
        {
            return score switch
            {
                int n when n > 10 => ProtonRating.Platinum,
                int n when n > 8 => ProtonRating.Gold,
                int n when n > 4 => ProtonRating.Silver,
                int n when n > 2 => ProtonRating.Bronze,
                _ => ProtonRating.Borked,
            };
        }

        private int GetScore(Dictionary<string, JsonElement> attributes)
        {
            int score = 0;
            foreach (var attribute in attributes.Keys)
            {
                if (attributes[attribute].ValueKind != JsonValueKind.String) continue;
                string? answer = attributes[attribute].GetString();
                if (answer != null)
                {
                    if (negativeYes.Contains(attribute) && answer.ToLower().Trim() == "no")
                    {
                        score += 1;
                    }
                    else if (positiveYes.Contains(attribute) && answer.ToLower().Trim() == "yes")
                    {
                        score += 1;
                    }
                }
            }
            return score;
        }

        public Task<bool> IsSteamGame(string progTitle)
        {
            if (pdb != null)
            {
                foreach (JsonElement element in pdb.RootElement.EnumerateArray())
                {
                    if (element.TryGetProperty("app", out JsonElement appElement) &&
                        appElement.TryGetProperty("title", out JsonElement titleElement) &&
                        titleElement.GetString() == progTitle)
                    {
                        return Task.FromResult(true);
                    }
                }
            }
            else
            {
                ErrorService.Log("Issue with PDB JSON, cannot be read for some reason. Extracting here anyway.");
                ExtractAndSetReport();
            }
            return Task.FromResult(false);
        }

        private static string ExtractAppId(string url) => ProtonAppIDRegex().Match(url)?.Groups[1].Value ?? string.Empty;

        public static async Task<string> GetID(string gameTitle)
        {
            var searchStr = $"{gameTitle} protondb";
            var res = await Searcher.SearchAsync(searchStr);
            string link = string.Empty;

            foreach (PageResult page in res.Pages)
            {
                link = page.Link;
                break;
            }

            return ExtractAppId(link);
        }

        public async Task<ProtonRating?> GetDirect(string gameTitle)
        {
            string id = await GetID(gameTitle);
            string query = $"https://www.protondb.com/api/v1/reports/summaries/{id}.json";
            var rawResponse = await client.Get(query);

            if (string.IsNullOrEmpty(rawResponse))
            {
                return null;
            }

            var normalizedResponse = rawResponse.ToLower();
            return normalizedResponse switch
            {
                var r when r.Contains(@"""tier"": ""platinum""") => ProtonRating.Platinum,
                var r when r.Contains(@"""tier"": ""gold""") => ProtonRating.Gold,
                var r when r.Contains(@"""tier"": ""silver""") => ProtonRating.Silver,
                var r when r.Contains(@"""tier"": ""bronze""") => ProtonRating.Bronze,
                var r when r.Contains(@"""tier"": ""borked""") => ProtonRating.Borked,
                _ => null
            };
        }

        [System.Text.RegularExpressions.GeneratedRegex(@"app/(\d+)")]
        private static partial System.Text.RegularExpressions.Regex ProtonAppIDRegex();
    }
}
