﻿using System.Text.Json;

namespace Teleport.Classification.Groups
{
    public static class FPHandler
    {
        public static async Task<JsonDocument> GetFlatpaks()
        {
            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string flathubJson = Path.Combine(appDataPath, "flatpaks.json");

            if (!File.Exists(flathubJson))
            {
                using var httpClient = new HttpClient();
                var response = await httpClient.GetAsync("https://flathub.org/api/v1/apps");
                using var stream = await response.Content.ReadAsStreamAsync();
                using var fileStream = File.Create(flathubJson);
                await stream.CopyToAsync(fileStream);
            }
            using (var fileStream = File.OpenRead(flathubJson))
            {
                return await JsonDocument.ParseAsync(fileStream);
            }
        }
    }
}