﻿using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using System.Text;
using Teleport.Web;

namespace Teleport.Local
{
    public static class IODBSetupService
    {
        public async static Task Go()
        {
            CacheService.CheckForAndFixCorruption();

            string tempDirectory = Path.Combine(Path.GetTempPath(), "pdb_temp_"),
                pathOfReportArchive = Path.Combine(tempDirectory, "reports_may5_2024.tar.gz"),
                protonReportUrl = "https://github.com/bdefore/protondb-data/raw/master/reports/reports_may5_2024.tar.gz";

            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);

            if (!File.Exists(pathOfReportArchive))
            {
                await new DWebClient().DownloadFile(protonReportUrl, pathOfReportArchive);
            }
            
            if (!File.Exists(Path.Combine(tempDirectory, "reports_piiremoved.json")))
            {
                using Stream inStream = File.OpenRead(pathOfReportArchive);
                using Stream gzipStream = new GZipInputStream(inStream);
                TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream, Encoding.UTF8);
                tarArchive.ExtractContents(tempDirectory);
            }
        }
    }
}
