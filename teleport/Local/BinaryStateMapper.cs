﻿using Teleport.Models;

namespace Teleport.Local
{
    public static class BinaryStateMapper
    {
        public static Status MapStringToState(string statString)
        {
            string normalized = statString.Trim().ToLower();
            Status result = new(StatusType.NONE);

            if (normalized.Contains("native"))
            {
                result.Type = StatusType.NATIVE;
                return result;
            }

            else if (normalized.Contains("proton"))
            {
                result.Type = StatusType.PROTON;
                if (normalized.Contains("bronze"))
                {
                    result.ProtonRating = Models.Ratings.ProtonRating.Bronze;
                }
                else if (normalized.Contains("silver"))
                {
                    result.ProtonRating = Models.Ratings.ProtonRating.Silver;
                }
                else if (normalized.Contains("gold"))
                {
                    result.ProtonRating = Models.Ratings.ProtonRating.Gold;
                }
                else if (normalized.Contains("platinum"))
                {
                    result.ProtonRating = Models.Ratings.ProtonRating.Platinum;
                }
                else
                {
                    result.ProtonRating = Models.Ratings.ProtonRating.Borked;
                }
                return result;
            }

            else if (normalized.Contains("yabridge"))
            {
                result.Type = StatusType.YABRIDGE;
                return result;
            }

            else
            {
                result.Type = StatusType.WINE;
                if (normalized.Contains("bronze"))
                {
                    result.WineRating = Models.Ratings.WineRating.Bronze;
                }
                else if (normalized.Contains("silver"))
                {
                    result.WineRating = Models.Ratings.WineRating.Silver;
                }
                else if (normalized.Contains("gold"))
                {
                    result.WineRating = Models.Ratings.WineRating.Gold;
                }
                else if (normalized.Contains("platinum"))
                {
                    result.WineRating = Models.Ratings.WineRating.Platinum;
                }
                else if (normalized.Contains("garbage"))
                {
                    result.WineRating = Models.Ratings.WineRating.Garbage;
                }
                else
                {
                    result.Type = StatusType.NONE;
                    result.WineRating = Models.Ratings.WineRating.None;
                }
                return result;
            }
        }
    }
}