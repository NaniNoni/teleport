﻿using System.Security.Cryptography;
using Teleport.Models;

namespace Teleport.Local
{
    public static class CacheService
    {
        public static void CheckForAndFixCorruption()
        {
            string oldVersionPath = Path.Combine(Path.GetTempPath(), "pdb_teleport_checker");
            if (!(File.Exists(Classification.Constants.CacheFPath) && File.Exists(Classification.Constants.CachedResultsFPath)))
            {
                ClearAll();
            }
            if (Directory.Exists(oldVersionPath))
            {
                ClearAll();
                Directory.Delete(oldVersionPath, true);
            }
        }

        private static void ClearAll()
        {
            if (File.Exists(Classification.Constants.CachedResultsFPath))
            {
                File.Delete(Classification.Constants.CachedResultsFPath);
            }
            if (File.Exists(Classification.Constants.CacheFPath))
            {
                File.Delete(Classification.Constants.CacheFPath);
            }
        }

        public static bool VerifyInstalledSoftwareHash(List<string> softwareList)
        {
            if (!File.Exists(Classification.Constants.CacheFPath))
            {
                return false;
            }
            var installedSoftwareHash = CalculateHash(softwareList);
            var storedHash = LoadStoredHash();
            return installedSoftwareHash.SequenceEqual(storedHash);
        }
        
        public static void SaveCompatMap(CompatibilityMap compatMap)
        {
            using BinaryWriter writer = new(File.Open(Classification.Constants.CachedResultsFPath, FileMode.Create));
            writer.Write(compatMap.Count());
            foreach (CompatibilityPair pair in compatMap.GetCompatibilityPairs())
            {
                writer.Write(pair.Name);
                writer.Write(pair.State.ToString());
            }
        }

        public static CompatibilityMap ReadSavedMap()
        {
            CompatibilityMap result = new();
            using (BinaryReader reader = new(File.Open(Classification.Constants.CachedResultsFPath, FileMode.Open)))
            {
                int count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    string key = reader.ReadString();
                    string value = reader.ReadString();
                    result.Add(new CompatibilityPair{
                        Name = key,
                        State = BinaryStateMapper.MapStringToState(value)
                    });
                }
            }
            return result;
        }

        public static void StoreHash(List<string> softwareList)
        {
            var fpath = Classification.Constants.CacheFPath;
            byte[]? hash = CalculateHash(softwareList);
            if (!File.Exists(fpath))
            {
                _ = Directory.CreateDirectory(Path.GetDirectoryName(fpath));
            }
            File.WriteAllBytes(fpath, hash);
        }

        private static byte[] CalculateHash(List<string> softwareList)
        {
            return SHA256.HashData(System.Text.Encoding.UTF8.GetBytes(string.Join(",", softwareList)));
        }

        private static byte[] LoadStoredHash()
        {
            var filePath = Classification.Constants.CacheFPath;
            return File.Exists(filePath) ? File.ReadAllBytes(filePath) : [];
        }
    }
}
