﻿using Spectre.Console;
using Teleport.Models;
using Teleport.Visuals.Internals;

namespace Teleport.Visuals
{
    public static class Bars
    {
        public async static Task<Tuple<BarChart, string>> GetBarsFromCompatMap(CompatibilityMap map)
        {
            BarChart barChart = new();
            Tuple<List<BarMetric>, string> bm = await BarGenerator.GenerateBars(map);

            foreach (var metric in bm.Item1)
            {
                if (metric.Value > 0)
                {
                    barChart.AddItem(metric);
                }
            }

            Tuple<BarChart, string> result = new(barChart, bm.Item2);
            return result;
        }
    }
}