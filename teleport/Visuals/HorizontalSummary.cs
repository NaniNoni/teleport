﻿using Spectre.Console;
using Teleport.Models;
using Teleport.Visuals.Internals;

namespace Teleport.Visuals
{
    public static class HorizontalSummary
    {
        public async static Task<Tuple<BreakdownChart, string>> GetHSummaryFromMap(CompatibilityMap map)
        {
            BreakdownChart chart = new();
            Tuple<List<HorizonMetric>, string> bm = await HorizonGenerator.GenerateHorizons(map);

            foreach (var metric in bm.Item1)
            {
                if (metric.Value > 0)
                {
                    chart.AddItem(metric);
                }
            }

            Tuple<BreakdownChart, string> result = new(chart, bm.Item2);
            return result;
        }
    }
}
