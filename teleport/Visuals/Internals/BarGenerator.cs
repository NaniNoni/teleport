﻿using Teleport.Models;

namespace Teleport.Visuals.Internals
{
    public static class BarGenerator
    {
        public static Task<Tuple<List<BarMetric>, string>> GenerateBars(CompatibilityMap map)
        {
            List<BarMetric> output = [];
            int nativeCount = 0,
                protPlatCount = 0,
                protGoldCount = 0,
                protSilverCount = 0,
                protBronzeCount = 0,
                yabridgeCount = 0,
                winePlatCount = 0,
                wineGoldCount = 0,
                wineSilverCount = 0,
                wineBronzeCount = 0,
                wineGarbCount = 0,
                protBorkedCount = 0,
                totalCount = 0,
                noCount = 0;

            foreach (CompatibilityPair pair in map.GetCompatibilityPairs())
            {
                totalCount++;
                if (pair.State.Type == StatusType.NATIVE)
                {
                    nativeCount++;
                }
                else if (pair.State.Type == StatusType.PROTON)
                {
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Platinum)
                    {
                        protPlatCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Gold)
                    {
                        protGoldCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Silver)
                    {
                        protSilverCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Bronze)
                    {
                        protBronzeCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Borked)
                    {
                        protBorkedCount++;
                    }
                }
                else if (pair.State.Type == StatusType.YABRIDGE)
                {
                    yabridgeCount++;
                }
                else if (pair.State.Type == StatusType.WINE)
                {
                    if (pair.State.WineRating == Models.Ratings.WineRating.Platinum)
                    {
                        winePlatCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Gold)
                    {
                        wineGoldCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Silver)
                    {
                        wineSilverCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Bronze)
                    {
                        wineBronzeCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Garbage)
                    {
                        wineGarbCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.None)
                    {
                        noCount++;
                    }
                }
                else if (pair.State.Type == StatusType.NONE)
                {
                    noCount++;
                }
            }

            output.Add(new BarMetric { Label = "[green]Native[/]", Value = nativeCount, Color = Spectre.Console.Color.Green});
            output.Add(new BarMetric { Label = "[white]Proton - Platinum[/]", Value = protPlatCount, Color = Spectre.Console.Color.White });
            output.Add(new BarMetric { Label = "[gold1]Proton - Gold[/]", Value = protGoldCount, Color = Spectre.Console.Color.Gold1 });
            output.Add(new BarMetric { Label = "[silver]Proton - Silver[/]", Value = protSilverCount, Color = Spectre.Console.Color.Silver });
            output.Add(new BarMetric { Label = "[sandybrown]Proton - Bronze[/]", Value = protBronzeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new BarMetric { Label = "[sandybrown](May work via yabridge)[/]", Value = yabridgeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new BarMetric { Label = "[white]Wine - Platinum[/]", Value = winePlatCount, Color = Spectre.Console.Color.White });
            output.Add(new BarMetric { Label = "[gold1]Wine - Gold[/]", Value = wineGoldCount, Color = Spectre.Console.Color.Gold1 });
            output.Add(new BarMetric { Label = "[silver]Wine - Silver[/]", Value = wineSilverCount, Color = Spectre.Console.Color.Silver });
            output.Add(new BarMetric { Label = "[sandybrown]Wine - Bronze[/]", Value = wineBronzeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new BarMetric { Label = "[red]Wine - Garbage[/]", Value = wineGarbCount, Color = Spectre.Console.Color.Red });
            output.Add(new BarMetric { Label = "[red]Proton - Borked[/]", Value = protBorkedCount, Color = Spectre.Console.Color.Red });
            output.Add(new BarMetric { Label = "[red3]No WineHQ entry found[/]", Value = noCount, Color = Spectre.Console.Color.Red3 });

            int willRun = totalCount - (protBorkedCount + wineGarbCount + noCount);
            string statString = $"{willRun}/{totalCount} ({(willRun * 100 / totalCount)}%) of currently installed programs will run on Linux.";
            Tuple<List<BarMetric>, string> result = new(output, statString);

            return Task.FromResult(result);
        }
    }
}
