﻿using Teleport.Models;

namespace Teleport.Visuals.Internals
{
    public class HorizonGenerator
    {
        public static Task<Tuple<List<HorizonMetric>, string>> GenerateHorizons(CompatibilityMap map)
        {
            List<HorizonMetric> output = [];
            int nativeCount = 0,
                protPlatCount = 0,
                protGoldCount = 0,
                protSilverCount = 0,
                protBronzeCount = 0,
                yabridgeCount = 0,
                winePlatCount = 0,
                wineGoldCount = 0,
                wineSilverCount = 0,
                wineBronzeCount = 0,
                wineGarbCount = 0,
                protBorkedCount = 0,
                totalCount = 0,
                noCount = 0;

            foreach (CompatibilityPair pair in map.GetCompatibilityPairs())
            {
                totalCount++;
                if (pair.State.Type == StatusType.NATIVE)
                {
                    nativeCount++;
                }
                else if (pair.State.Type == StatusType.PROTON)
                {
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Platinum)
                    {
                        protPlatCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Gold)
                    {
                        protGoldCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Silver)
                    {
                        protSilverCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Bronze)
                    {
                        protBronzeCount++;
                    }
                    if (pair.State.ProtonRating == Models.Ratings.ProtonRating.Borked)
                    {
                        protBorkedCount++;
                    }
                }
                else if (pair.State.Type == StatusType.YABRIDGE)
                {
                    yabridgeCount++;
                }
                else if (pair.State.Type == StatusType.WINE)
                {
                    if (pair.State.WineRating == Models.Ratings.WineRating.Platinum)
                    {
                        winePlatCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Gold)
                    {
                        wineGoldCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Silver)
                    {
                        wineSilverCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Bronze)
                    {
                        wineBronzeCount++;
                    }
                    if (pair.State.WineRating == Models.Ratings.WineRating.Garbage)
                    {
                        wineGarbCount++;
                    }
                }
                else
                {
                    noCount++;
                }
            }

            output.Add(new HorizonMetric { Label = "Native", Value = nativeCount, Color = Spectre.Console.Color.Green });
            output.Add(new HorizonMetric { Label = "Proton - Platinum", Value = protPlatCount, Color = Spectre.Console.Color.White });
            output.Add(new HorizonMetric { Label = "Proton - Gold", Value = protGoldCount, Color = Spectre.Console.Color.Gold1 });
            output.Add(new HorizonMetric { Label = "Proton - Silver", Value = protSilverCount, Color = Spectre.Console.Color.Silver });
            output.Add(new HorizonMetric { Label = "Proton - Bronze", Value = protBronzeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new HorizonMetric { Label = "(May work via yabridge)", Value = yabridgeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new HorizonMetric { Label = "Wine - Platinum", Value = winePlatCount, Color = Spectre.Console.Color.White });
            output.Add(new HorizonMetric { Label = "Wine - Gold", Value = wineGoldCount, Color = Spectre.Console.Color.Gold1 });
            output.Add(new HorizonMetric { Label = "Wine - Silver", Value = wineSilverCount, Color = Spectre.Console.Color.Silver });
            output.Add(new HorizonMetric { Label = "Wine - Bronze", Value = wineBronzeCount, Color = Spectre.Console.Color.SandyBrown });
            output.Add(new HorizonMetric { Label = "Wine - Garbage", Value = wineGarbCount, Color = Spectre.Console.Color.Red });
            output.Add(new HorizonMetric { Label = "Proton - Borked", Value = protBorkedCount, Color = Spectre.Console.Color.Red });
            output.Add(new HorizonMetric { Label = "No WineHQ entry found", Value = noCount, Color = Spectre.Console.Color.Red3 });

            int willRun = totalCount - (protBorkedCount + wineGarbCount + noCount);
            string statString = $"{willRun}/{totalCount} of currently installed programs will run on Linux.";
            Tuple<List<HorizonMetric>, string> result = new(output, statString);

            return Task.FromResult(result);
        }
    }
}
