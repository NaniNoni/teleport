﻿using Spectre.Console;

namespace Teleport.Visuals.Internals
{
    public sealed class BarMetric : IBarChartItem
    {
        public required string Label { get; set; }
        public double Value { get; set; }
        public Color? Color { get; set; }
    }
}
