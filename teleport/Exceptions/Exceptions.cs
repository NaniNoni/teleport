﻿using System.Diagnostics;

namespace Teleport.Exceptions
{
    public static class ErrorService
    {
        private static List<string> Errors = [], Infos = [];

        public static void Log(string prepend, Exception? e = null, string extraData = "")
        {
            var suffix = string.Empty;
            if (e != null)
            {
                suffix = $": {e.Message}";
            }
            string error = $"{prepend}{suffix}";
            if (!string.IsNullOrEmpty(extraData))
            {
                error += $"\n{extraData}";
            }
            Errors.Add(error);
            Debug.WriteLine(error);
        }

        public static void LogInfo(string info)
        {
            Infos.Add(info);
            Debug.WriteLine(info);
        }

        public static (List<string>, List<string>) GetDebugInfo()
        {
            return (Errors, Infos);
        }

        private static void RemoveDuplicateEntries()
        {
            Infos = Infos.Distinct().ToList();
            Errors = Errors.Distinct().ToList();
        }

        public static void ShowAll()
        {
            RemoveDuplicateEntries();
            Console.Clear();
            Console.WriteLine("There's a problem.");
            if (Infos.Count > 0)
            {
                Console.WriteLine("\n\nInfos:\n");
                foreach (var info in Infos)
                {
                    Console.WriteLine(info);
                }
            }
            if (Errors.Count > 0)
            {
                Console.WriteLine("\n\nErrors:\n");
                foreach (var error in Errors)
                {
                    Console.WriteLine(error);
                }
            }
            Console.WriteLine("\nReport the whole error message here: https://www.gitlab.com/navid-m/teleport/issues");
        }
    }
}