﻿using ShellProgressBar;
using Spectre.Console;
using Teleport.Classification;
using Teleport.Exceptions;
using Teleport.Finders;
using Teleport.Local;
using Teleport.Models.Meta;

namespace Teleport
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "--about")
            {
                Meta.ShowAbout();
                return;
            }

            Initialize();

            Table tab = new();
            Tuple<BarChart, string>? bars = null;
            // Tuple<BreakdownChart, string>? bds = null;


            var options = new ProgressBarOptions
            {
                ProgressCharacter = '─',
                ProgressBarOnBottom = true,
                ForegroundColor = ConsoleColor.DarkCyan,
            };

            using (var pbar = new ProgressBar(100, "Loading, wait.", options))
            {
                try
                {
                    await IODBSetupService.Go();
                    var installedSoftwareTask = Task.Run(() => Grabber.GetInstalledSoftware());
                    var classifyTask = new Classifier().ClassifyAllProgramsAsync(await installedSoftwareTask);
                    while (!classifyTask.IsCompleted)
                    {
                        await Task.WhenAny(Task.Delay(5), classifyTask);
                        pbar.Tick();
                    }
                    await classifyTask;
                    bars = await Visuals.Bars.GetBarsFromCompatMap(classifyTask.Result);
                    //bds = await Visuals.HorizontalSummary.GetHSummaryFromMap(classifyTask.Result);
                    tab = classifyTask.Result.ToTable();
                }
                catch (Exception ex)
                {
                    ErrorService.Log("Error in main", ex);
                }
            }

            // Output formatting.
            AnsiConsole.Clear();
            AnsiConsole.Write(tab.Width(85));
            AnsiConsole.Write("\n");
            
            if (bars != null)
            {
                var actualBarChart = bars.Item1;
                AnsiConsole.Write("\n");
                AnsiConsole.Write(new Panel(actualBarChart.Width(81)).Header("Overall"));
                AnsiConsole.Write(new Text("\n" + bars.Item2));
                AnsiConsole.Write("\n");
            }
            else
            {
                ErrorService.ShowAll();
            }

/*          if (bds != null)
            {
                var actualChart = bds.Item1;
                AnsiConsole.Write("\n");
                AnsiConsole.Write(new Panel(actualChart.Width(81)).Header("Overall"));
                AnsiConsole.Write(new Text("\n" + bds.Item2));
                AnsiConsole.Write("\n");
            }
*/
            Console.ReadKey();
        }

        static void Initialize()
        {
            Console.Title = "Teleport";
            AnsiConsole.Clear();

            using var httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromSeconds(4);
            try
            {
                var response = httpClient.GetAsync("https://1.1.1.1").Result;
                response.EnsureSuccessStatusCode();
            }
            catch
            {
                AnsiConsole.WriteLine("Internet connection is required to check compatibility. Stopping.");
                Environment.Exit(1);
            }
        }
    }
}
